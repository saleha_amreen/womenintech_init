import {iDbService} from "../database/idbservice";
import {DbCollection} from "../database/DbCollection";
import { Injectable } from '@angular/core';

@Injectable()
export class DbService<T extends DbCollection> implements iDbService<T> {

    get(collection_name: string, collection_id?: string) {
        var data = new Array<T>();
        console.log('get is called');
        return data;
    }
    post(collection: Array<T>) {
        return new Array<T>();
    }
    put(collection_value: T) {
        return <T> new DbCollection();
    }
    delete(collection:T) {
        return "Successfully deleted!"
    }
}