"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var DbCollection_1 = require("../database/DbCollection");
var core_1 = require('@angular/core');
var DbService = (function () {
    function DbService() {
    }
    DbService.prototype.get = function (collection_name, collection_id) {
        var data = new Array();
        console.log('get is called');
        return data;
    };
    DbService.prototype.post = function (collection) {
        return new Array();
    };
    DbService.prototype.put = function (collection_value) {
        return new DbCollection_1.DbCollection();
    };
    DbService.prototype.delete = function (collection) {
        return "Successfully deleted!";
    };
    DbService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], DbService);
    return DbService;
}());
exports.DbService = DbService;
//# sourceMappingURL=DbService.js.map