export interface iDbService<T> {
    get(collection_name: string, collection_id?: string): Array<T>;
    post(collection: Array<T>): Array<T>;
    put(collection_value: T): T;
    delete(collection:T):string;
}

