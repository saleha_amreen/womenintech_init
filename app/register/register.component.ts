import  {Component} from '@angular/core';

@Component({
    selector:'wit-register',
    templateUrl:'./app/register/registerTemplate.html'
})

export class RegisterComponent{
    firstName: string;
    lastName: string;
    userPassword:string;
    securityQuestion:string;
    password:string;

    Register(){
        alert('Welcome ' + this.firstName + ' ' + this.lastName + ' to WIT Application!');
    }
}