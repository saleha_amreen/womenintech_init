import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { loginRoutes } from './login/login.routes';

// Route Configuration
export const routes: Routes = [
  {
    path: '',
    redirectTo: '/events',
    pathMatch: 'full'
  },
    // Add login routes form a different file
  ...loginRoutes
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
