import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {RegisterComponent} from './register/register.component'
import {LoginComponent} from './login/login.component';
import {EventsComponent} from './events/events.component';
import { FormsModule }   from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {LoginService} from './login/login.service';
import {DbService} from "./database/DbService";
import { routing } from './app.routes';

@NgModule({
  imports:      [ BrowserModule, FormsModule, NgbModule, routing ],
  declarations: [AppComponent,RegisterComponent, LoginComponent, EventsComponent],
  bootstrap: [AppComponent,RegisterComponent, LoginComponent],
  providers: [LoginService, DbService]
})

export class AppModule { }
