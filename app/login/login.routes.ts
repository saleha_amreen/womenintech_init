
import { Routes } from '@angular/router';
import { EventsComponent } from '../events/events.component';

// Route Configuration
export const loginRoutes: Routes = [
  { path: 'events', component: EventsComponent }
];