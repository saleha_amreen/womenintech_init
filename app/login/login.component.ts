import {Component} from '@angular/core';
import {Router} from '@angular/router'
import {LoginService} from '../login/login.service';

@Component({
    selector: 'wit-login',
    templateUrl: './app/login/loginTemplate.html'
})

export class LoginComponent {
    private _loginService: LoginService;
    private _router: Router;

    constructor(loginService: LoginService, router: Router){
        this._loginService = loginService;
        this._router = router;
    }
    userEmail:string;
    password:string;
    SubmitLogin() {
        // var result = this._loginService.Login(this.userEmail, this.password);
        // alert(result);
        console.log('about to route');
        this._router.navigate(['/events']);
    }
}