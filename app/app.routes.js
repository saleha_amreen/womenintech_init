"use strict";
var router_1 = require('@angular/router');
var login_routes_1 = require('./login/login.routes');
// Route Configuration
exports.routes = [
    {
        path: '',
        redirectTo: '/events',
        pathMatch: 'full'
    }
].concat(login_routes_1.loginRoutes);
exports.routing = router_1.RouterModule.forRoot(exports.routes);
//# sourceMappingURL=app.routes.js.map