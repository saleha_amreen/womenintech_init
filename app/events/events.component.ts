import {Component} from '@angular/core';

@Component({
    selector:'wit-events',
    templateUrl:'./app/events/eventsTemplate.html'
})

export class EventsComponent{
    eventName:string;
    eventDate:string;
    eventTime:string;
    eventDetails:string;
}