module.exports = function(config) {
    config.set({
        basePath: '',
        plugins: [
            require('karma-jasmine'),
            require('karma-chrome-launcher'),
            require('karma-jasmine-html-reporter'), // click "Debug" in browser to see it
            require('karma-htmlfile-reporter') // crashing w/ strange socket error
        ],
        frameworks: ['jasmine'],
        files: [
            'tests/**/*spec.js'
        ],
        exclude: [
        ],
        preprocessors: {},
        reporters: ['progress','dots', 'html'],
        // HtmlReporter configuration
        htmlReporter: {
            // Open this file to see results in browser
            outputFile: 'tests/coverage.html',
            // Optional
            pageTitle: 'WIT - Code Coverage',
            subPageTitle: __dirname
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['Chrome'],
        singleRun: false
    });
};